//B1: Import Thư viện express
//import { Express } from "express";
const express = require("express");

//Import thư viện mongoose
var mongoose = require('mongoose');

//b2 khởi tạo app express
const app = new express();

//b3: khai báo cỗng để chạy api
const port = 8008;

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));

//Khai báo các model
const drinkModel = require("./app/model/drinkModel");
const voucherModel = require("./app/model/voucherModel");
const orderModel = require("./app/model/orderModel");
const userModel = require("./app/model/userModel");

const drinkRouter = require("./app/routes/drinkRouter")
const voucherRouter = require("./app/routes/voucherRouter")
const userRouter = require("./app/routes/userRouter")
const orderRouter = require("./app/routes/orderRouter")


//cấu hình để sử dụng json
app.use(express.json());

//Middleware
//Middleware console log ra thời gian hiện tại 
app.use((req, res, next) => {
    console.log("Thời gian hiện tại:", new Date());

    next();
})

//middleware console log ra request method
app.use((req, res, next) => {
    console.log("Request method", req.method);

    next();
})

//Khai báo các api 

//Sử dụng router
app.use("/api/v1/drinks", drinkRouter);
app.use("/api/v1/vouchers", voucherRouter);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/orders", orderRouter);

//b4: star app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})