const getAllDrinksMiddlewares = (req, res, next) => {
    console.log("Get all drinks Middleware");

    next();
}

const createDrinksMiddlewares = (req, res, next) => {
    console.log("Create drinks Middleware");

    next();
}

const getDetailDrinksMiddlewares = (req, res, next) => {
    console.log("Get detail drink Middleware");

    next();
}

const updateDrinksMiddlewares = (req, res, next) => {
    console.log("Update drinks Middleware");

    next();
}

const deleteDrinksMiddlewares = (req, res, next) => {
    console.log("Delete drinks Middleware");

    next();
}

module.exports = {
    getAllDrinksMiddlewares,
    createDrinksMiddlewares, 
    getDetailDrinksMiddlewares,
    updateDrinksMiddlewares,
    deleteDrinksMiddlewares
}