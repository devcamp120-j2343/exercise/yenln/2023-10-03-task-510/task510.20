const mongoose = require("mongoose");

const userModel = require("../model/userModel");
const userRouter = require("../routes/userRouter");

const createUser = async (req, res) => {
    const {
        fullName,
        email,
        address,
        phone
    } = req.body;

    //b2 validate dự liệu

    //fullName phải nhập
    if (!email) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "email is not valid"
        })
    }
    if (!address) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "address is not valid"
        })
    }
    if (!phone) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "phone is not valid"
        })
    }
    if (!fullName) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "fullName is not valid"
        })
    }
    //Thao tác với CSDL
    var newUser = {
        _id: new mongoose.Types.ObjectId(),
        email,
        address,
        phone,
        fullName
    }

    userModel.create(newUser)
        .then((data) => {
            return res.status(201).json({
                status: "Create new user sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const getAllUser = async (req, res) => {

    userModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(201).json({
                    status: "Get all users sucessfully",
                    data
                })
            }
            else {
                return res.status(404).json({
                    status: "Not found any user",
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const getUserById = async (req, res) => {
    var userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    userModel.findById(userId)
        .then((data) => {
            if (!data) {
                return res.status(404).json({
                    status: "Not found detail of this user",
                    data
                })
            }
            else {
                return res.status(201).json({
                    status: `Get user ${userId} sucessfully`,
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const updateUserById = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    const { fullName, email, phone, address } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    // Nếu stars là undefined => vẫn hợp lệ
    // Nếu stars khác undefined và không thỏa mãn điều kiện hợp lệ => return lỗi 
    if (fullName == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is invalid!"
        })
    }
    if (email == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "email is invalid!"
        })
    }
    if (phone == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is invalid!"
        })
    }
    if (address == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "address is invalid!"
        })
    }


    //B3: thực thi model
    try {
        let updateUser = {
            fullName,
            email,
            phone,
            address
        }

        const updatedUser = await userModel.findByIdAndUpdate(userId, updateUser);

        if (updateUser) {
            return res.status(200).json({
                status: "Update review sucessfully",
                data: updateUser
            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteUserById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        const deletedUser = await userModel.findByIdAndDelete(userId);

        if (deletedUser) {
            return res.status(200).json({
                status: `Delete user ${userId} sucessfully`,
                data: deletedUser
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}
