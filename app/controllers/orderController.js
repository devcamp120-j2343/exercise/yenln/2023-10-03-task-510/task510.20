const mongoose = require("mongoose");

const userModel = require("../model/userModel");
const userRouter = require("../routes/userRouter");
const orderModel = require("../model/orderModel");

const voucherController = require("../controllers/voucherController");
const drinkController = require("../controllers/drinkController");

const createOrder = async (req, res) => {
    const {
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status,
        userId,
        orderCode
    } = req.body;

    //b2 validate dự liệu
    if (orderCode == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "orderCode is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    //fullName phải nhập
    if (!pizzaSize) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizzaSize is not valid"
        })
    }
    if (!pizzaType) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizzaType is not valid"
        })
    }
    if (!status) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizzaType is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucher is not exist!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(drink)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drink is invalid!"
        })
    }
    //Thao tác với CSDL

    try {
        var newOrder = {
            _id: new mongoose.Types.ObjectId(),
            pizzaSize,
            pizzaType,
            voucher,
            drink,
            status,
            orderCode
        };

        const createOrder = await orderModel.create(newOrder);

        const updateUser = await userModel.findByIdAndUpdate(userId, {
            $push: { orders: createOrder }
        })

        if (!updateUser) {
            return res.status(404).json({
                message: "Co loi xay ra",
            });
        }
        return res.status(201).json({
            message: "Create order successfully",
            user: updateUser,
            data: createOrder
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })

    }

}

const getAllOrder = async (req, res) => {

    orderModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(201).json({
                    status: "Get all Orders sucessfully",
                    data
                })
            }
            else {
                return res.status(404).json({
                    status: "Not found any Order",
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const getOrderById = async (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is invalid!"
        })
    }

    orderModel.findById(orderId)
        .then((data) => {
            if (!data) {
                return res.status(404).json({
                    status: "Not found detail of this order",
                    data
                })
            }
            else {
                return res.status(201).json({
                    status: `Get order ${orderId} sucessfully`,
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const updateOrderById = async (req, res) => {
    //B1: thu thập dữ liệu
    const orderId = req.params.orderId;

    const {
        orderCode,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        userId
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is invalid!"
        })
    }

    // Nếu stars là undefined => vẫn hợp lệ
    // Nếu stars khác undefined và không thỏa mãn điều kiện hợp lệ => return lỗi 
    if (orderCode == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "orderCode is invalid!"
        })
    }
    if (pizzaSize == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaSize is invalid!"
        })
    }
    if (pizzaType == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaType is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucher is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(drink)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drink is invalid!"
        })
    }



    //B3: thực thi model
    try {
        let updateOrder = {
        }
        if(orderCode){
            updateOrder.orderCode = orderCode
        }
        if(pizzaSize){
            updateOrder.pizzaSize = pizzaSize
        }
        if(pizzaType){
            updateOrder.pizzaType = pizzaType
        }
        if(voucher){
            updateOrder.voucher = voucher
        }
        if(drink){
            updateOrder.drink = drink
        }

        const updatedOrder = await orderModel.findByIdAndUpdate(orderId, updateOrder);

        if (updateOrder) {
            return res.status(200).json({
                status: "Update order sucessfully",
                data: updateOrder
            })
        } else {
            return res.status(404).json({
                status: "Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteOrderById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var orderId = req.params.orderId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is invalid!"
        })
    }

    try {
        const deletedOrder = await orderModel.findByIdAndDelete(orderId);

        if (deletedOrder) {
            return res.status(200).json({
                status: `Delete order ${orderId} sucessfully`,
                data: deletedOrder
            })
        } else {
            return res.status(404).json({
                status: "Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}
