//import { Express } from "express";
const express = require("express");

const orderMiddleware = require("../middlewares/order.middleware");

const orderController = require("../controllers/orderController");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", orderController.getAllOrder);

router.post("/:userid/orders", orderController.createOrder);

router.get("/:orderId", orderController.getOrderById);

router.put("/:orderId", orderController.updateOrderById);

router.delete("/:orderId", orderController.deleteOrderById);

module.exports = router;