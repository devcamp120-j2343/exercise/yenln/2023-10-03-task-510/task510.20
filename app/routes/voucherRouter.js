//import { Express } from "express";
const express = require("express");

const voucherMiddleware = require("../middlewares/voucher.middleware");

const voucherController = require("../controllers/voucherController");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", voucherController.getAllVoucher)

router.post("/", voucherController.createVoucher)

router.get("/:voucherId", voucherController.getVoucherById)

router.put("/:voucherId", voucherController.updateVoucherById)

router.delete("/:voucherId", voucherController.deleteVoucherById)

module.exports = router;