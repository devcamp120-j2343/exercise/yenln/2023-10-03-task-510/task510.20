//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
        unique: true,
    },
    orders: [
        {
            type: mongoose.Types.ObjectId,
            ref: "order",
        }
    ],
})

//B4: biên dịch schema thành model
module.exports = mongoose.model("user", userSchema)