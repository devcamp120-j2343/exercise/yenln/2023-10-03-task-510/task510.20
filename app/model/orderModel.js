//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true,
    },
    pizzaSize: {
        type: String,
        required: true,
    },
    pizzaType: {
        type: String,
        required: true,
    },
    voucher: [
        {
            type: mongoose.Types.ObjectId,
            ref: "voucher",
        }
    ],
    drink: [
        {
            type: mongoose.Types.ObjectId,
            ref: "drink",
        }
    ],
    pizzaType: {
        type: String,
        required: true,
    },
})

//B4: biên dịch schema thành model
module.exports = mongoose.model("order", orderSchema)