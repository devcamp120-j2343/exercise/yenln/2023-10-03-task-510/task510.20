//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const voucherSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    maVoucher: {
        type: String,
        required: true,
        unique: true,
    },
    phanTramGiamGia: {
        type: Number,
        required: true,
    },
    ghiChu: {
        type: String,
        required: false,
    },
})

//B4: biên dịch schema thành model
module.exports = mongoose.model("voucher", voucherSchema)